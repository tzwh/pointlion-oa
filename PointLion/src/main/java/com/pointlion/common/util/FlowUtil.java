package com.pointlion.common.util;

import cn.hutool.json.JSONObject;
import com.pointlion.common.constant.CommonConstants;

import java.util.HashMap;
import java.util.Map;

/***
 * @des
 * @author Ly
 * @date 2022/7/17
 */

public class FlowUtil {

    /****
     * 组装通用申请的流程变量
     * @param title
     * @param username
     * @param nickname
     * @param applytime
     * @return
     */
    public static Map<String,Object> buildFlowVars(String title,String username,String nickname,String applytime,String iscustomform){
        Map<String,Object> vars = new HashMap<>();
        JSONObject obj = new JSONObject();
        obj.set(CommonConstants.APPLY_VARS_TITLE,title);
        obj.set(CommonConstants.APPLY_VARS_USERNAME,username);
        obj.set(CommonConstants.APPLY_VARS_NICKNAME,nickname);
        obj.set(CommonConstants.APPLY_VARS_APPLYTIME,applytime);
        obj.set(CommonConstants.APPLY_VARS_ISCUSTOMFORM, iscustomform);
        //obj.set(CommonConstants.APPLY_VARS_BUSINESSTABLENAME,businessTableName);
        //流程变量
        vars.put(CommonConstants.APPLY_VARS_ORDERINFO,obj.toString());//单据信息
        return vars;
    }

}
