package com.pointlion.common.util;

import java.util.Base64;

/***
 * @des
 * @author Ly
 * @date 2022/7/16
 */

public class Base64Util {

    /****
     * 字节转base64
     * @param byteArray
     * @return
     */
    public static String byte2Base64(byte [] byteArray){
        return Base64.getEncoder().encodeToString(byteArray);
    }
}
