package com.pointlion.back.home.service;

import com.pointlion.back.home.mapper.HomeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/***
 * @des
 * @author Ly
 * @date 2023/6/4
 */

@Service
public class HomeService {

    @Autowired
    private HomeMapper homeMapper;

    public Map<String,Long> getItemCountData(){
        return homeMapper.getItemCountData();
    }
}
