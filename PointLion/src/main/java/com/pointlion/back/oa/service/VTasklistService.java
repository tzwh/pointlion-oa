package com.pointlion.back.oa.service;

import java.util.ArrayList;
import java.util.List;

import com.pointlion.back.oa.domain.VTasklist;
import com.pointlion.common.service.base.BaseService;
import com.pointlion.back.oa.mapper.VTasklistMapper;
import com.pointlion.common.constant.BillTypeEnum;
import com.pointlion.common.utils.StringUtils;
import org.flowable.engine.HistoryService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 我的待办Service业务层处理
 * 
 * @author pointLion
 * @date 2022-07-16
 */
@Service
public class VTasklistService extends BaseService
{
    @Autowired
    private VTasklistMapper vTasklistMapper;
    @Autowired
    private TaskService taskService;
    @Autowired
    private FlowProcessInstanceService flowProcessInstanceService;
    @Autowired
    private HistoryService historyService;
    @Autowired
    private VTasklistService vTasklistService;

    /**
     * 查询我的待办
     * 
     * @param taskid 我的待办主键
     * @return 我的待办
     */
    public VTasklist selectVTasklistByTaskid(String taskid)
    {
        return vTasklistMapper.selectVTasklistByTaskid(taskid);
    }

    /**
     * 查询我的待办列表
     * 
     * @param vTasklist 我的待办
     * @return 我的待办
     */
    public List<VTasklist> selectVTasklistList(VTasklist vTasklist)
    {
        return vTasklistMapper.selectVTasklistList(vTasklist);
    }


    /****
     * 查询审批历史
     * @param insId
     * @return
     */
    public List<VTasklist> selectHisByInsId(String insId){
        List<VTasklist> list =  vTasklistMapper.selectHisByInsId(insId);
        for(VTasklist t:list){
            t.setNickName(t.getNickName()+"["+t.getAssignee()+"]");
            if(StringUtils.isNotBlank(t.getDueTime())){
                t.setCompleteStatus("已完成");
            }else{
                t.setCompleteStatus("未审批");
            }
        }
        return list;
    }

    /****
     * 查询审批历史记录
     * @param businessId
     * @param billType
     * @return
     */
    public List<VTasklist> getHisByBusinessId(Long businessId,String billType){
        List<VTasklist> result = new ArrayList<>();
        String businessKey = billType+":"+ BillTypeEnum.getTableName(billType)+":"+businessId;
        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery().processInstanceBusinessKey(businessKey).orderByProcessInstanceStartTime().asc().list();
        for(HistoricProcessInstance his:list){
            String insId = his.getId();
            List<VTasklist> l = vTasklistService.selectHisByInsId(insId);
            result.addAll(l);
        }
        return result;
    }


    /****
     * 查询待办任务列表
     * @param username
     * @return
     */
    public List<VTasklist> selectTodoTaskList(String username){
        return vTasklistMapper.selectTodoTaskList(username);
    }


    /*****
     * 完成任务
     * @param taskId
     */
    public void commitTask(String taskId,String insId,String comment){
        taskService.addComment(taskId,insId,"【同意】"+comment);
        taskService.complete(taskId);
    }


    /*****
     * 驳回任务
     * @param taskId
     */
    public void rejectTask(String taskId,String insId,String comment){
        taskService.addComment(taskId,insId,"【驳回】"+comment);
        flowProcessInstanceService.stopProcessInstanceById(insId);
    }



}
