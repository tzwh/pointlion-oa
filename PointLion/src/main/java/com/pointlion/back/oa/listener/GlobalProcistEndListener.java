package com.pointlion.back.oa.listener;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.pointlion.back.oa.mapper.FlowProcessMapper;
import com.pointlion.common.constant.CommonConstants;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEntityEvent;
import org.flowable.engine.delegate.event.AbstractFlowableEngineEventListener;
import org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/***
 * @des
 * 流程结束全局监听器
 * @author Ly
 * @date 2023/5/31
 */

@Component
public class GlobalProcistEndListener extends AbstractFlowableEngineEventListener {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FlowProcessMapper flowProcessMapper;

    @Override
    protected void processCompleted(FlowableEngineEntityEvent event) {
        logger.info("进入流程完成监听器------------------------Start---------------------->");
        String eventName = event.getType().name();

        FlowableEntityEventImpl flowableEntityEvent = (FlowableEntityEventImpl) event;
        ExecutionEntityImpl processInstance = (ExecutionEntityImpl) flowableEntityEvent.getEntity();
        //获取结束类型----如果是驳回的话，类型会是reject
        Object finishType = processInstance.getVariable(CommonConstants.FLOW_FINISH_TYPE);
        String status = CommonConstants.FLOW_STATUS_FINISH;
        String insId = null;
        if(finishType!=null){
            if(CommonConstants.FLOW_FINISH_TYPE_REJECT.equals(finishType.toString())){
                status = CommonConstants.FLOW_STATUS_REJECT;
                insId = "";//流程id清空
            }
        }


        Object orderinfo = processInstance.getVariable(CommonConstants.APPLY_VARS_ORDERINFO);
        if(orderinfo!=null){
            String orderinfoStr = orderinfo.toString();
            JSONObject order = JSONObject.parseObject(orderinfoStr);
            if(order!=null){
                //String iscustomform = order.getString(CommonConstants.APPLY_VARS_ISCUSTOMFORM);
                String businessKey = processInstance.getBusinessKey();
                String tablename = businessKey.split(":")[1];
                String businessId = businessKey.split(":")[2];
                //String businessId2 = processInstance.getProcessInstanceBusinessKey();
                if(StrUtil.isNotBlank(tablename) && StrUtil.isNotBlank(businessId)){
                    Long id = Long.parseLong(businessId);
                    flowProcessMapper.updateBusinessBillStatus(tablename,status,insId,id);
                }
            }
        }

        Date startTime = processInstance.getStartTime();
        String processDefinitionKey = processInstance.getProcessDefinitionKey();
        String processInstanceId = processInstance.getProcessInstanceId();
        String processInstanceBusinessKey = processInstance.getProcessInstanceBusinessKey();
        int suspensionState = processInstance.getSuspensionState();


        logger.info("流程事件类型->{}", eventName);
        logger.info("流程开始时间->{}", startTime);
        logger.info("流程定义Key->{}", processDefinitionKey);
        logger.info("流程实例ID->{}", processInstanceId);
        logger.info("流程业务key->{}", processInstanceBusinessKey);
        logger.info("流程是否挂起标志->{}", suspensionState);

        logger.info("流程完成监听器------------------------End---------------------->");
    }

}