package com.pointlion.back.oa.mapper;

import com.pointlion.back.oa.domain.ActReProcdef;

import java.util.List;

/**
 * 流程定义Mapper接口
 * 
 * @author pointLion
 * @date 2022-07-09
 */
public interface ActReProcdefMapper
{
    /**
     * 查询流程
     * 
     * @return 流程定义
     */
    public List<ActReProcdef> selectAllFlowList();


}
