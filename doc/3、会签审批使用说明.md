### 会签审批使用说明

会签审批为，一个任务，有，【不定数量】的人进行审批。
1、选择多实例
![输入图片说明](img/多实例/1.png)
2、弹窗中选择配置
nrOfInstances：实例总数。<br/>
nrOfActiveInstances：当前活动的（即未完成的），实例数量。对于顺序多实例，这个值总为1。<br/>
nrOfCompletedInstances：已完成的实例数量。<br/>
loopCounter：给定实例在for-each循环中的index。<br/>
<br/>
此处设置的变量为userList，单据中的所选用户传入流程图参与审批
可以使用上面描述的固定流程变量配置完成比例，比方10个人参与审批，可以设置nrOfCompletedInstances/nrOfInstances>0.5，即有一半以上的人审批通过即可完成该任务。
![输入图片说明](img/多实例/2.png)
3、另外可以设置，会签的，人串行和并行审批。串行的图标为横着的三条线，并行为竖着的三条线
![输入图片说明](img/多实例/3.png)